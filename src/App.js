import logo from './logo.svg';
import './App.css';
import { Header } from './Components/header/Header';
import { Sidebar } from './Components/sidebar/Sidebar';
import { Dashboard } from './Components/dashboard/Dashboard';

function App() {
  return (
    <div className="app">
      <div className= "content-header" >
      <Header/>
      </div>
      <div className= "main-content">
        <div className= "content-sidebar">
        <Sidebar/>
        </div>
        <div className= "content-dashboard">
        <Dashboard/>
        </div>
      </div>
    </div>
    
  );
}

export default App;
