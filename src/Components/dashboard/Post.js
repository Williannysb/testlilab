import React, { useState, useEffect } from "react";
import axios from "axios";
import likeimg from './../../icons/heart.svg';
import commentsimg from './../../icons/chat-dots.svg';
import linkimg from './../../icons/link.svg';
import { Modal } from "react-bootstrap";
import { CommentsModal } from "./CommentsModal";

const BASE_URL = 'https://dummyapi.io/data/api';
const APP_ID = '6081d1c08e63e484c0f2d9d7';

export const Post = ({ post }) => {
    const [showC,setshowC] = useState(false)
    const [comments, setcomments] = useState([])

    useEffect(() => {
        getComments();
    }, [])

    const handleShowC = () => {setshowC(!showC)}

    const getComments = async () => {
        let url = `${BASE_URL}/post/${post.id}/comment`;
        const { data } = await axios.get(url, {
            headers: { 'app-id': APP_ID },
        });
        setcomments(data.data)
    }

    console.log(post)
    return (
        <div className="post">
        <Modal show={showC} onHide ={handleShowC} centered>
            <CommentsModal comments={comments}/>
        </Modal>
            <div className="post-header">
                <div className="post-user-text">
                    <img src={post.owner.picture} className='user-img' />
                    <h2> {post.owner.firstName} {post.owner.lastName}</h2>
                </div>
                <div className="post-date-text">
                    {post.publishDate}
                </div>
            </div>
            <div className="post-body">
                <img src={post.image} className="post-img" />
            </div>
            <div className="post-footer">
                <div className="post-bottons">
                    <div className="Numero">
                       <img className= "likes" src={likeimg}/>
                       <h4> {post.likes}</h4>
                    </div>
                    <div className= "Numero"  onClick={handleShowC}>
                    <img className= "comments" src={commentsimg}/>
                    <h4> {comments.length}</h4>
                    </div>
                    <div className="Numero">
                    <img className= "link" src={linkimg}/>
                    </div>

                </div>
                <div className="post-info">
                    <div className="post-info-text">
                        {post.text}
                    </div>
                    <div className="post-info-tags">
                        {post.tags.map((tag) => (
                            <div className="post-tag" key={tag}>
                                {tag}
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}
