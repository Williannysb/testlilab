import React, { useState, useEffect } from "react";
import axios from "axios";
import { Post } from "./Post";

const BASE_URL = 'https://dummyapi.io/data/api';
const APP_ID = '6081d1c08e63e484c0f2d9d7';

export const Postlist = () => {

    const[postlist, setpostlist] = useState([])
    const[postlisttoshow, setpostlisttoshow] = useState([])

    useEffect(() => {
        if(postlist.length===0){
            getData();
        }
    }, [postlist])

    const getData = async() =>{
        let url = `${BASE_URL}/post?page=1&limit=15`;
        const { data } = await axios.get(url, {
            headers: { 'app-id': APP_ID },
        });
        setpostlist(data.data)
    }

    return (
        <div className="post-list">
        {postlist.map((postitem) => (
            <Post key={postitem.id} post={postitem} />
        ))}
            
        </div>
    )
}
