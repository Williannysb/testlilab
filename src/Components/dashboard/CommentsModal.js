import React from 'react'

export const CommentsModal = ({comments}) => {
    console.log(comments)
    return (
        <div className="">
        {comments.map((com)=>(
                <div className="coment">
                <img src={com.owner.picture} className='picture' />
                <h5> {com.owner.firstName} {com.owner.lastName}</h5>
                <div>
                    {com.message}
                    </div>
            </div>
        ))}
        </div>
    )
}
